﻿using RestfullService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RestfullService.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DavetiyeFormu()
        {
            return View();
        }

        //IsValid gerekli koşullarısağlıyor mu mesela davetiyemodeldeki required alanlar girilmiş mi?

        [HttpPost]
        public ActionResult DavetiyeFormu(DavetiyeModel model)
        {
            if (ModelState.IsValid)
            {
                Veritabanı.Add(model);
                return View("Tesekkurler", model);
            }
            return View(model);
        }
        public ActionResult Katilanlar()
        {
            var katilim = Veritabanı.Liste.Where(i => i.KatilmaDurumu == true);
            return PartialView(katilim);
        }
    }
}