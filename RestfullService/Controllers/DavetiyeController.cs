﻿using RestfullService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestfullService.Controllers
{
    public class DavetiyeController : ApiController
    {
        [HttpGet]
        //httpget yerine metot başına get yazarak da yapabiliriz,yani GetKatilanlar
        public IEnumerable<DavetiyeModel> Katilanlar()
        {
            return Veritabanı.Liste.Where(i => i.KatilmaDurumu == true);
        }

        public IEnumerable<DavetiyeModel> GetKatilmayanlar()
        {
            return Veritabanı.Liste.Where(i => i.KatilmaDurumu == false);
        }


        //Birden fazla get veya post metodu olduğunda hangi metodu çalıştıracağını 
        //anlaması için routeconfig ayarında action da yer alır.

        [HttpPost]
        //PostEkle
        public void Ekle(DavetiyeModel model)
        {
            if(ModelState.IsValid)
            {
                Veritabanı.Add(model);
            }
        }
    }
}
