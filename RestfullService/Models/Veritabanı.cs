﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestfullService.Models
{
    public static class Veritabanı
    {
        private static Dictionary<string, DavetiyeModel> _liste;

        static Veritabanı()
        {
            _liste = new Dictionary<string, DavetiyeModel>();

            _liste.Add("Ayse", new DavetiyeModel
            {
                Ad = "Ayse",
                Soyad = "Ercin",
                Eposta = "aercin@gmail.com",
                KatilmaDurumu = true

            });
            _liste.Add("Ali", new DavetiyeModel
            {
                Ad = "Ali",
                Soyad = "Destioglu",
                Eposta = "adestioglu@gmail.com",
                KatilmaDurumu = true

            });
            _liste.Add("Eren", new DavetiyeModel
            {
                Ad = "Eren",
                Soyad = "Eroglu",
                Eposta = "eeroglu@gmail.com",
                KatilmaDurumu = false

            });

            _liste.Add("Gözde", new DavetiyeModel
            {
                Ad = "Gozde",
                Soyad = "Kardas",
                Eposta = "gkardas@gmail.com",
                KatilmaDurumu = false

            });
        }

        public static void Add(DavetiyeModel model)
        {
            string key = model.Ad.ToLower();
            if(_liste.ContainsKey(key))
            {
                _liste[key] = model;
            }
            else
            {
                _liste.Add(key, model);
            }
        }

        //private olan elamanı get ile dışarıya sunacak
        public static IEnumerable<DavetiyeModel> Liste
        {
            get { return _liste.Values; }
        }
    }
}